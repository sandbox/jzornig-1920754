
(function($) {

	$(document).on({
		"mousedown touchstart" : function() {
			$(this).siblings("input").trigger("start");
		},
		"mouseup touchend" : function() {
			$(this).siblings("input").trigger("stop");
		},
		"mousemove touchmove" : function() {
			$(this).siblings("input").trigger("change");
		}
	}, "#book_slider_slider #book_slider_previous");
	
	$(document).bind("mobileinit", function(){
		  $.extend(  $.mobile , {
			  ajaxEnabled: false
		  });
		});
	
	$(document).on("pageinit",
			function() {
				console.log('hello');
				// Locate current page.
				var book_slider_currpage = book_slider_menulinks.indexOf(window.location.pathname);
				console.log(book_slider_menulinks);
				console.log(window.location.pathname);
				console.log(book_slider_currpage);

				// Hide the Tooltip at first.
				var tooltip = $('input#book_slider_slider');
				tooltip.hide();
				// Call the Slider.
				var slider = $('#book_slider_slider');
				slider.slider({
					start : function(event, ui) {
					},
					stop : function(event, ui) {
					},
				});
				slider.attr({
					min : 1,
					max : book_slider_menulinks.length,
					step : 1,
					value : book_slider_currpage + 1,
				});
				$('#book_slider_form a.ui-btn.ui-slider-handle').prepend('<div id="bubble"> </div>');
				$('#book_slider_form #bubble').hide();
				slider.on('slidestart', function(event, ui) {
					$('#book_slider_form #bubble').fadeIn('fast');
				});
				slider.on('slidestop', function(event, ui) {
					tooltip.fadeOut('fast');
					var pos = event.target.value - 1;
					if (pos != book_slider_currpage) {
						window.location.href = book_slider_menulinks[pos];
					}
				});
				slider.on('change',
						function(event, ui) {
							var pos = event.target.value - 1;
							$('#book_slider_form #bubble').html('<span>' + book_slider_menutitles[pos] + '</span>');
							$('#book_slider_form #bubble').css('left', -$('#book_slider_form #bubble').width() / 2);
							$('#book_slider_form .ui-btn-text').text(event.target.value);
						});
				slider.slider('refresh');
				$('#book_slider_previous').on('click', function(event) {
					event.stopPropagation();
					if (book_slider_currpage != 0) {
						window.location.href = book_slider_menulinks[book_slider_currpage - 1];
					}
				});
				$('#book_slider_next').on('click', function(event) {
					event.stopPropagation();
					if (book_slider_currpage != book_slider_menulinks.length) {
						window.location.href = book_slider_menulinks[book_slider_currpage + 1];
					}
				});
				$('body').on('click touchend', function(event, ui) {
					$('#book_slider_form').slideToggle('fast');
				});
			});

})(jQuery);
